import { FC } from "react";
import { Button } from "patterns";
import { HomeStyles, AppLogo } from "./home.css";
import logo from "assets/logo.svg";

export const Home: FC = () => {
  return (
    <HomeStyles>
      <AppLogo src={logo} className="App-logo" alt="logo" />
      <p>
        Edit <code>src/App.tsx</code> and save to reload.
      </p>
      <Button action={() => window.open("https://reactjs.org")}>Learn React</Button>
    </HomeStyles>
  );
};

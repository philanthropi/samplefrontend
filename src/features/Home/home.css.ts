import styled from "styled-components/macro";
// import { vars } from "styles/vars.css";

export const HomeStyles = styled.div`
  background-color: #282c34;
  color: white;
  font-size: calc(10px + 2vmin);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
  text-align: center;
`;

export const AppLogo = styled.img`
  height: 40vmin;
  pointer-events: none;

  @keyframes App-logo-spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }

  @media (prefers-reduced-motion: no-preference) {
    animation: App-logo-spin infinite 20s linear;
  }
`;
